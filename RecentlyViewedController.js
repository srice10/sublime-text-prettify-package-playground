function RecentlyViewedController($q, $scope, $rootScope, $http, $timeout, horseshoeService, qdimService, loadStateModel, recentlyViewedModel)
{
   $scope.loadStateModel = loadStateModel;
   $scope.recentlyViewedModel = recentlyViewedModel;
   $scope.staticData = null;


   /************
    ** METHODS **
    ************/

   /***************************************************************
    ** receiveError
    **
    ** receives an error from the launcher service
    ***************************************************************/
   $scope.receiveError = function(data)
   {
      $scope.loadStateModel.setStateError($scope.recentlyViewedModel);
   };


   /***************************************************************
    ** receiveRecentlyViewed
    **
    ** handle recently viewed static data loading event
    ***************************************************************/
   $scope.receiveData = function(results)
   {
      $scope.staticData = results[0];

      $scope.getClassData();
   };


   /***************************************************************
    ** fetch classList data for when on qdim app home pages
    **
    ***************************************************************/
   $scope.getClassData = function()
   {
      /////////////////////////////////////////////////////////////////////////////////////
      // if there is a qdimAppApiBase defined, then fetch the app definition to get the  //
      // list of tables in the app and subsequently limit the recently viewed shown      //
      /////////////////////////////////////////////////////////////////////////////////////
      if(typeof qdimAppApiBase != "undefined")
      {
         horseshoeService.getHttpGetPromise(qdimAppApiBase + "definition")
            .then($scope.receiveClassListOK, $scope.receiveClassListError);
      }
      else
      {
         //////////////////////////////////////////////////////////////////////////////////////////////
         // else, if there is no qdimAppApiBase (ie, we're in launcher), then call directly          //
         // to getRecentlyViewed with out setting tableData (which it will interpret to mean 'all')  //
         //////////////////////////////////////////////////////////////////////////////////////////////
         $scope.getRecentlyViewed();
      }
   };


   /***************************************************************
    ** if in qdim, receieve class list data to limit types of filters returned
    **
    ***************************************************************/
   $scope.receiveClassListOK = function(results)
   {
      var tableData = results.data.tables;

      if(tableData)
      {
         $scope.recentlyViewedModel.tableData = tableData;
      }

      //////////////////////////
      // get recently viewed  //
      //////////////////////////
      $scope.getRecentlyViewed();
   };


   /***************************************************************
    ** receive classList error
    *
    ***************************************************************/
   $scope.receiveClassListError = function()
   {
      console.log("Did not receive any entity class names.");

      /////////////////////////////////
      // get recently viewed records //
      /////////////////////////////////
      $scope.getRecentlyViewed();
   };


   /***************************************************************
    ** make ajax call to get recently viewed
    **
    ***************************************************************/
   $scope.getRecentlyViewed = function()
   {
      ///////////////////////////////////////////////////////////////////////////////////
      // If in rf mode, recordTypeNameList will be set in rf recently viewed directive //
      ///////////////////////////////////////////////////////////////////////////////////
      if($scope.recentlyViewedModel.tableData)
      {
         for(var i = 0; i < $scope.recentlyViewedModel.tableData.length; i++)
         {
            $scope.recentlyViewedModel.recordTypeNameList.push($scope.recentlyViewedModel.tableData[i].fullEntityClassName);
         }
      }

      qdimService.getRecentlyViewedPromise($scope.limit, $scope.recentlyViewedModel.recordTypeNameList)
         .then($scope.getRecentlyViewedResponseOK, $scope.getRecentlyViewedResponseError);
   };


   /***************************************************************
    ** receive recently viewed data
    **
    ***************************************************************/
   $scope.getRecentlyViewedResponseOK = function(results)
   {
      console.log("Received OK response from get-recently-viewed");

      var recentlyViewedCandidates = results.data;
      $scope.recentlyViewedModel.recentlyViewed = [];

      ///////////////////////////////////////////////////////////////////////
      // iterate over the retrieved recently viewed, fetching complete     //
      // app data from our static info - update model with recently viewed //
      ///////////////////////////////////////////////////////////////////////
      for(var i = 0; i < recentlyViewedCandidates.length; i++)
      {
         var recentlyViewed = recentlyViewedCandidates[i];

         //////////////////////////////////////////////////////////////////
         // if in rf mode, all results go into the recently viewed model //
         //////////////////////////////////////////////////////////////////
         if($scope.rfMode)
         {
            $scope.recentlyViewedModel.recentlyViewed.push(recentlyViewed);
         }
         else
         {
            //////////////////////////////////////////////////////////
            // get the sections from the currently displayed screen //
            //////////////////////////////////////////////////////////
            var currentSections = [];
            var url = window.location.href;
            var parts = url.split("/");
            var lastTerm = parts[parts.length - 1];
            var secondToLastTerm = parts[parts.length - 2];

            for(var k = 0; k < $scope.staticData.screens.length; k++)
            {
               if(lastTerm == $scope.staticData.screens[k].name)
               {
                  currentSections = $scope.staticData.screens[k].sections;
               }
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // If on account-management or warehouse-operations, check the apps within the allowed sections to see if recently viewed should be displayed on this screen. //
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if(currentSections)
            {
               for(var l = 0; l < currentSections.length; l++)
               {
                  var apps = currentSections[l].apps;
                  for(var m = 0; m < apps.length; m++)
                  {
                     if(apps[m] == recentlyViewed.applicationName)
                     {
                        $scope.recentlyViewedModel.recentlyViewed.push(recentlyViewed);
                     }
                  }
               }
            }

            ///////////////////////////////////////////////////////////////////////////////////////
            // If on app home page, verify the recently viewed against the url's last substring, //
            // which is secondToLastTerm because trailing slash in the url creates emtpy term    //
            ///////////////////////////////////////////////////////////////////////////////////////
            if(secondToLastTerm == recentlyViewed.applicationName)
            {
               $scope.recentlyViewedModel.recentlyViewed.push(recentlyViewed);
            }
         }
      }

      if($scope.recentlyViewedModel.recentlyViewed.length > 0)
      {
         $scope.loadStateModel.setStateOk($scope.recentlyViewedModel);
      }
      else
      {
         $scope.loadStateModel.setStateEmpty($scope.recentlyViewedModel);
      }

      /////////////////////////////////////
      // initially toggle to "Show More" //
      /////////////////////////////////////
      $scope.recentlyViewedModel.showMoreRecentRecordsText = true;

      ////////////////////////////////////
      // set limit filter for ng-repeat //
      ////////////////////////////////////
      $scope.recentlyViewedModel.clickLimit = 5;
   };


   /***************************************************************
    ** receive an 'error' from the save call
    **
    ***************************************************************/
   $scope.getRecentlyViewedResponseError = function(results)
   {
      console.log("Received ERROR response from get-recently-viewed-records");
      $scope.loadStateModel.setStateError($scope.recentlyViewedModel);
   };


   /***************************************************************
    ** recentlyViewedOnClick
    **
    ** handle event for when recently-viewed record is clicked on
    ***************************************************************/
   $scope.recentlyViewedOnClick = function(recentlyViewed)
   {
      window.location.href = recentlyViewed.path;
   };


   /***************************************************************
    ** ajax call to show more recent records
    ***************************************************************/
   $scope.showMoreRecentRecords = function()
   {
      ///////////////////////////
      // toggle to "Show Less" //
      ///////////////////////////
      $scope.recentlyViewedModel.showMoreRecentRecordsText = false;

      ////////////////////////////////////
      // set limit filter for ng-repeat //
      ////////////////////////////////////
      $scope.recentlyViewedModel.clickLimit = $scope.recentlyViewedModel.initialLimit;
   };


   /***************************************************************
    **ajax call to show less recent records
    ***************************************************************/
   $scope.showLessRecentRecords = function()
   {
      ///////////////////////////
      // toggle to "Show More" //
      ///////////////////////////
      $scope.recentlyViewedModel.showMoreRecentRecordsText = true;

      ////////////////////////////////////
      // set limit filter for ng-repeat //
      ////////////////////////////////////
      $scope.recentlyViewedModel.clickLimit = 5;
   };


   /***************************************************************
    ** event handler to hide Show More until more to show
    **
    ***************************************************************/
   $scope.showButtons = function()
   {
      if(loadStateModel.isStateOk(recentlyViewedModel) && $scope.recentlyViewedModel.recentlyViewed.length > 5)
      {
         return true;
      }
      else
      {
         return false;
      }
   };


   /*********
    ** INIT **
    *********/

   ////////////////////////////////////////////////////////////////////
   // load the static data and recently-viewed records for this page //
   // this function is called in the link parameter of the directive //
   ////////////////////////////////////////////////////////////////////
   $q.all([horseshoeService.getPermissedStaticDataPromise()])
      .then($scope.receiveData, $scope.receiveError);

   $scope.recentlyViewedModel.initialLimit = $scope.limit;
   $scope.loadStateModel.setStateLoading($scope.recentlyViewedModel);
}
