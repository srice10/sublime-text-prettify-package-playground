angular.module('qdim.directives', ['webCoreDirectivesModule'])

/****************************************************************
 ** Directive to generate content (tabs and main section) of qdim pages
 **
 ****************************************************************/
.directive("ipQdimContent", function()
{
   return(
   {
      restrict: 'E',
      controller: QDIMContentController,
      template: " <div class='tabbable launcher-tabs qdim-tabs'> " +
         "    <ul ng-cloak id='primaryNavTabs' class='nav nav-tabs'> " +
         "       <li class='qdim-dashboard'><a href='home' id='qdimTab-home' data-toggle='tab' ng-click='homeTabClicked()'><span class='tab-rotate'><i class='ip-glyphs {{qdimContentModel.app.icon}}'></i></span></a></li> " +
         "       <li ng-repeat='openTable in qdimContentModel.openTables' ng-click='tableTabClicked(openTable)' style='width: 120px; top: {{(($index+1) * 120)-25}}px;'>" +
         "          <a id='qdimTab-{{openTable.path}}' data-toggler'tab'><span class='tab-rotate'><button type='button' class='close' style='float: none;' aria-hidden='true' ng-click='closeTableTab(openTable)'>&times;</button>{{openTable.label}}</span></a> " +
         "       </li> " +
         "    </ul> " +
         "    <div id='myTabContent' class='tab-content'> " +
         "       <div class='tab-pane active' id='dashboardTab'> " +
         "          <div ng-view></div> " +
         "       </div> " +
         "    </div> " +
         " </div> "
   });
})


/****************************************************************
 ** Directive to generate criteria filter dropdown
 **
 ****************************************************************/
.directive("ipCriteriaDropdown", function()
{
   return(
   {
      restrict: 'E',
      controller: CriteriaDropdownController,
      scope:
      {
         field: '=',
         fieldName: '=',
         tooltipPlacement: '@'
      },
      templateUrl: qdimCoreBase + "app/templates/criteria-dropdown.html"
   });
})


/****************************************************************
 ** Directive to generate criteria filter simple radio option
 **
 ****************************************************************/
.directive("ipCriteriaDropdownSimpleTags", function(tagsService, qdimService)
{
   return(
   {
      restrict: 'E',
      scope:
      {
         field: '=',
         criteriaDropdownModel: '=',
         label: '@',
         operator: '@',
         focusFunction: '&',
         changeFunction: '&',
         key: '@',
         inputMask: '@',
         tooltipText: '='
      },
      template: " <div class='radio tags-quickbox-container'> " +
         "    <label> " +
         "       <input value='{{operator}}' ng-click='focusFunction()' ng-model='criteriaDropdownModel.fieldMap[field.name].operator' name='{{field.name}}-criteria-options' type='radio' ng-show='criteriaDropdownModel.fieldMap[field.name].isAdvanced'> " +
         "       {{label}} " +
         "    </label> " +
         "    <span ng-class='{\"is-advanced\": criteriaDropdownModel.fieldMap[field.name].isAdvanced}'> " +

      '       <ip-quickbox ' +
         '          element-id="tags-quickbox-{{operator}}" element-classes="tag full-width" bound-field="criteriaDropdownModel[field.name].values" ' +
         '          tags-mode="true" allow-create-tags="true" tag-suggestion-url-handler="getTagSuggestionURL" ' +
         '          tag-selected-handler="addTagHandler" tag-removed-handler="deleteTagHandler" ' +
         '       ></ip-quickbox> ' +

      "    </span> " +
         " </div> ",
      link: function(scope, element, attributes, ctrl)
      {

         /****************************************************************************
          ** ajax call to get tag suggestions
          **
          ***************************************************************************/
         scope.getTagSuggestionURL = function(event)
         {
            console.log("in directive getTagSuggestionURL");
            return(tagsService.getTagSuggestionURL());
         };


         /****************************************************************************
          ** event handler for when a tag is added
          **
          ***************************************************************************/
         scope.addTagHandler = function(event)
         {
            console.log("Added tag [" + event.object.text + "]");

            /////////////////////////////////////////////////////////////////////
            // add the tag (its text) to the criteria model for the tags field //
            /////////////////////////////////////////////////////////////////////
            var tagsFieldCriterialModel = scope.criteriaDropdownModel.fieldMap[scope.field.name]
            if(!tagsFieldCriterialModel.values)
            {
               tagsFieldCriterialModel.values = [];
            }
            var values = tagsFieldCriterialModel.values;

            /////////////////////////////////////////////////////////
            // only add the value if it isn't already in the array //
            /////////////////////////////////////////////////////////
            var needToAdd = true;
            for(var i = 0; i < values.length; i++)
            {
               if(values[i] == event.object.text)
               {
                  needToAdd = false;
                  break;
               }
            }

            if(needToAdd)
            {
               tagsFieldCriterialModel.values.push(event.object.text);
            }

            //////////////////////////////////
            // issue event about the update //
            //////////////////////////////////
            // qdimService.criteriaDropdownUpdated({"fieldName": scope.field.name, "operator": tagsFieldCriterialModel.operator, "values": tagsFieldCriterialModel.values});
         };


         /****************************************************************************
          ** event handler for when a tag is deleted
          **
          ***************************************************************************/
         scope.deleteTagHandler = function(event)
         {
            console.log("Removed tag [" + event.choice.text + "]");

            //////////////////////////////////////////////////////////////////////////
            // remove the tag (its text) from the criteria model for the tags field //
            //////////////////////////////////////////////////////////////////////////
            var tagsFieldCriterialModel = scope.criteriaDropdownModel.fieldMap[scope.field.name]
            if(tagsFieldCriterialModel.values)
            {
               var values = tagsFieldCriterialModel.values;
               for(var i = 0; i < values.length; i++)
               {
                  if(values[i] == event.choice.text)
                  {
                     values.splice(i, 1);
                  }
               }
            }

            //////////////////////////////////
            // issue event about the update //
            //////////////////////////////////
            // qdimService.criteriaDropdownUpdated({"fieldName": scope.field.name, "operator": tagsFieldCriterialModel.operator, "values": tagsFieldCriterialModel.values});
         };


      }
   });
})


/****************************************************************
 ** Directive to generate criteria filter simple radio option
 **
 ****************************************************************/
.directive("ipCriteriaDropdownSimpleRadio", function()
{
   return(
   {
      restrict: 'E',
      scope:
      {
         field: '=',
         criteriaDropdownModel: '=',
         label: '@',
         operator: '@',
         focusFunction: '&',
         changeFunction: '&',
         key: '@',
         inputMask: '@',
         tooltipText: '='
      },
      template: " <div class='radio'> " +
         "    <label> " +
         "       <input value='{{operator}}' ng-click='focusFunction()' ng-model='criteriaDropdownModel.fieldMap[field.name].operator' name='{{field.name}}-criteria-options' type='radio' ng-show='criteriaDropdownModel.fieldMap[field.name].isAdvanced'> " +
         "       {{label}} " +
         "    </label> " +
         "    <span><input tooltip-trigger='focus' tooltip='{{criteriaDropdownModel.fieldTooltip}}' id='{{key}}' ui-mask='{{inputMask}}' type='text' ng-change='changeFunction()' ng-focus='focusFunction()' ng-model='criteriaDropdownModel.fieldMap[field.name].values[0]' class='input-sm' ng-class=\"(criteriaDropdownModel.currentTextInputType==operator) ? '' : 'transparentColor'\"></span> " +
         " </div> "
   });
})


/****************************************************************
 ** Directive to generate criteria filter simple date option
 **
 ****************************************************************/
.directive("ipCriteriaDropdownSimpleDate", function()
{
   return(
   {
      restrict: 'E',
      scope:
      {
         field: '=',
         criteriaDropdownModel: '=',
         label: '@label',
         operator: '@operator',
         openFunction: '&',
         focusFunction: '&',
         changeFunction: '&',
         blurFunction: '&',
         key: '@'
      },
      template: " <div class='radio'> " +
         "    <label> " +
         "       <input value='{{operator}}' ng-click='focusFunction()' ng-model='criteriaDropdownModel.fieldMap[field.name].operator' name='{{field.name}}-criteria-options' type='radio' ng-show='criteriaDropdownModel.fieldMap[field.name].isAdvanced'> " +
         "       {{label}} " +
         "    </label> " +
         "    <span class='control-group control-group-sm input-append'> " +
         "       <input id='{{key}}' ng-focus='focusFunction()' ng-change='changeFunction()' ng-blur='blurFunction()' ng-click='openFunction()' ng-model='criteriaDropdownModel.fieldMap[field.name].values[0]' is-open='criteriaDropdownModel.calendarOpened[key]' type='text' show-weeks='false' datepicker-popup='M/d/yyyy' ng-class=\"(criteriaDropdownModel.currentTextInputType==operator) ? '' : 'transparentColor'\" /> " +
         "       <button class='btn' ng-focus='focusFunction()' ng-click='openFunction()'><i class='icon-calendar'></i></button> " +
         "    </span> " +
         " </div> "
   });
})


/****************************************************************
 ** Directive to generate favorite records section of pages
 **
 ****************************************************************/
.directive("ipFavoriteRecords", function()
{
   return(
   {
      restrict: 'E',
      controller: FavoriteRecordsController,
      scope:
      {
         limit: '@'
      },
      template: " <div class='row'>" +
         "    <div class='col-xs-10'><h4 class='launcher-subhead'>Favorite Records</h4></div>" +
         " </div>" +
         " <hr class='section-rule widget'>" +
         " <div class='collapse in recently-viewed' id='favorite-records1'>" +
         "    <div class='col-md-12'>" +

      "       <div class='row' ng-show='loadStateModel.isStateLoadingSlow(favoriteRecordsModel)'>" +
         "          <div class='loading-wrapper'><img class='loading' src='" + webCoreBase + "/img/spinner-white.gif'> <span class='loading-text'>Your records are loading...</span></div>" +
         "       </div>" +

      "       <div class='row' ng-show='loadStateModel.isStateEmpty(favoriteRecordsModel)'>" +
         "          Don't worry, looks you don't have any favorite records yet.  Once you do, they'll appear here." +
         "       </div>" +

      "       <div class='row' ng-show='loadStateModel.isStateError(favoriteRecordsModel)'>" +
         "          <div class='alert alert-danger no-hover'><strong>Oops! </strong>Troubles loading your favorite records right now...</div>" +
         "       </div>" +

      "       <div class='row'>" +
         "          <ul class='list-unstyled list-block' ng-show='loadStateModel.isStateOk(favoriteRecordsModel)'>" +
         "             <li class='col-md-12' ng-repeat='favoriteRecord in favoriteRecordsModel.favoriteRecords | limitTo: favoriteRecordsModel.clickLimit track by $index'>" +
         "                <div class='favorite-button'>" +
         "                   <input type='checkbox' name='{{favoriteRecord.id}}' ng-model='favoriteRecordsModel.isFavoriteRecord[favoriteRecord.id]'" +
         "                     ng-click='changeFavoriteRecordHome(favoriteRecord.id, favoriteRecord.entityId, favoriteRecord.entityTypeName)' class='favorite-button-checkbox' id='favoriteRecord{{favoriteRecord.id}}'>" +
         "                       <label class='favorite-button-label small' for='favoriteRecord{{favoriteRecord.id}}'>" +
         "                          <span ng-if='favoriteRecordsModel.isFavoriteRecord[favoriteRecord.id]' class='star on' tooltip='Remove from Favorites' tooltip-placement='top'></span>" +
         "                          <span ng-if='!(favoriteRecordsModel.isFavoriteRecord[favoriteRecord.id])' class='star off' tooltip='Add to Favorites' tooltip-placement='top'></span>" +
         "                       </label>" +
         "                 </div>" +
         "                 <div>" +
         "                    <a href='" + pathPrefix + "{{favoriteRecord.path}}'>" +
         "                       <span ng-if='!(favoriteRecord.description)' class='apps-links-name ng-binding'>{{favoriteRecord.entityLabel}}</span>" +
         "                       <span ng-if='favoriteRecord.description' class='apps-links-name ng-binding' tooltip='{{favoriteRecord.description}}' tooltip-placement='top'>{{favoriteRecord.entityLabel}}</span>" +
         "                    </a>" +
         "                 </div>" +
         "             </li>" +
         "          </ul>" +
         "       </div>" +

      "    </div>" +
         "    <div ng-show='showButtons()'>" +
         "        <button type='button' class='btn btn-default btn-block' ng-if='favoriteRecordsModel.showMoreFavoriteRecordsText' ng-click='showMoreFavoriteRecords()'>" +
         "           <span>Show more</span>" +
         "        </button>" +

      "         <button type='button' class='btn btn-default btn-block' ng-if='!(favoriteRecordsModel.showMoreFavoriteRecordsText)' ng-click='showLessFavoriteRecords()'>" +
         "           <span>Show less</span>" +
         "        </button>" +
         "    </div>" +
         " </div>",
      link: function(scope, element, attributes, ctrl)
      {
         scope.getData(attributes.limit);
      }
   });
})


/****************************************************************
 ** Directive to generate watched records section of pages
 **
 ****************************************************************/
.directive("ipWatchedRecords", function()
{
   return(
   {
      restrict: 'E',
      controller: WatchedRecordsController,
      scope:
      {
         limit: '@'
      },
      template: " <div class='row'>" +
         "    <div class='col-xs-10'><h4 class='launcher-subhead'>Watched Records</h4></div>" +
         " </div>" +
         " <hr class='section-rule widget'>" +
         " <div class='collapse in recently-viewed' id='watched-records1'>" +
         "    <div class='col-md-12'>" +

      "       <div class='row' ng-show='loadStateModel.isStateLoadingSlow(watchedRecordsModel)'>" +
         "          <div class='loading-wrapper'><img class='loading' src='" + webCoreBase + "/img/spinner-white.gif'> <span class='loading-text'>Your records are loading...</span></div>" +
         "       </div>" +

      "       <div class='row' ng-show='loadStateModel.isStateEmpty(watchedRecordsModel)'>" +
         "          You haven't viewed any records lately. When you do, they'll appear here." +
         "       </div>" +

      "       <div class='row' ng-show='loadStateModel.isStateError(watchedRecordsModel)'>" +
         "          <div class='alert alert-danger no-hover'><strong>Oops! </strong>Troubles loading your watched records right now...</div>" +
         "       </div>" +

      "       <div class='row'>" +
         "          <ul class='list-unstyled list-block' ng-show='loadStateModel.isStateOk(watchedRecordsModel)'>" +

      "             <li class='col-md-6' ng-repeat='watchedRecord in watchedRecordsModel.watchedRecords | limitTo: watchedRecordsModel.clickLimit track by $index'>" +
         "                <div class='favorite-button'>" +
         "                   <input type='checkbox' name='{{watchedRecord.id}}' ng-model='watchedRecordsModel.isWatchedRecord[watchedRecord.id]'" +
         "                     ng-click='changeWatchedRecordHome(watchedRecord.id, watchedRecord.entityId, watchedRecord.entityTypeName)' class='favorite-button-checkbox' id='watchedRecord{{watchedRecord.id}}'>" +
         "                       <label class='favorite-button-label' for='watchedRecord{{watchedRecord.id}}'>" +
         "                          <i ng-if='watchedRecordsModel.isWatchedRecord[watchedRecord.id]' class='glyphicon glyphicon-eye-open' tooltip='Remove from Watches' tooltip-placement='top'></i>" +
         "                          <i ng-if='! (watchedRecordsModel.isWatchedRecord[watchedRecord.id])' class='glyphicon glyphicon-eye-close' tooltip='Add to Watches' tooltip-placement='top'></i>" +
         "                       </label>" +
         "                 </div>" +
         "                 <div>" +
         "                    <a href='" + pathPrefix + "{{watchedRecord.path}}'><span class='apps-links-name ng-binding'>{{watchedRecord.entityLabel}}</span></a>" +
         "                 </div>" +

      "             </li>" +
         "          </ul>" +
         "       </div>" +
         "    </div>" +
         "    <div ng-show='showButtons()'>" +
         "        <button type='button' class='btn btn-default btn-block' ng-if='watchedRecordsModel.showMoreWatchedRecordsText' ng-click='showMoreWatchedRecords()'>" +
         "           <span>Show more</span>" +
         "        </button>" +

      "         <button type='button' class='btn btn-default btn-block' ng-if='!(watchedRecordsModel.showMoreWatchedRecordsText)' ng-click='showLessWatchedRecords()'>" +
         "           <span>Show less</span>" +
         "        </button>" +
         "    </div>" +
         " </div>",
      link: function(scope, element, attributes, ctrl)
      {
         scope.getData(attributes.limit);
      }
   });
})


/****************************************************************
 ** Directive to generate recently viewed (records) section of pages
 **
 ****************************************************************/
.directive("ipRecentlyViewedRecords", function()
{
   return(
   {
      restrict: 'E',
      controller: RecentlyViewedController,
      scope:
      {
         limit: '@'
      },
      template: " <div class='row'>" +
         "    <div class='col-xs-10'><h4 class='launcher-subhead'>Recently Viewed</h4></div>" +
         " </div>" +
         " <hr class='section-rule widget'>" +
         " <div class='collapse in recently-viewed' id='recent-filters1'>" +
         "    <div class='col-md-12'>" +

      "       <div class='row' ng-show='loadStateModel.isStateLoadingSlow(recentlyViewedModel)'>" +
         "          <div class='loading-wrapper'><img class='loading' src='" + webCoreBase + "/img/spinner-white.gif'> <span class='loading-text'>Your records are loading, please wait...</span></div>" +
         "       </div>" +

      "       <div class='row' ng-show='loadStateModel.isStateEmpty(recentlyViewedModel)'>" +
         "          Don't worry, looks like you haven't viewed any records yet.  Once you do, they'll appear here." +
         "       </div>" +

      "       <div class='row' ng-show='loadStateModel.isStateError(recentlyViewedModel)'>" +
         "          <div class='alert alert-danger no-hover'><strong>Oops! </strong>Troubles loading your recently viewed records right now...</div>" +
         "       </div>" +

      "       <div class='row'>" +
         "          <ul class='list-unstyled list-block' ng-show='loadStateModel.isStateOk(recentlyViewedModel)'>" +
         "             <li class='col-md-6' ng-repeat='recentlyViewed in recentlyViewedModel.recentlyViewed | limitTo: recentlyViewedModel.clickLimit track by $index' ng-click='recentlyViewedOnClick(recentlyViewed)'>" +
         "                <i class='ip-glyphs {{staticData.apps[recentlyViewed.applicationName].icon}}' style='color: #05609A; margin-right:0px; font-size:150%; top:4px;'></i> <a href='" + pathPrefix + "{{recentlyViewed.path}}'>" +
         "                   <span ng-if='recentlyViewed.entityLabel' class='apps-links-name ng-binding'>{{recentlyViewed.entityLabel}}</span>" +
         "                   <span ng-if='!(recentlyViewed.entityLabel)' class='apps-links-name ng-binding'>{{recentlyViewed.tableLabel}} {{recentlyViewed.entityId}}</span>" +
         "                </a>" +
         "             </li>" +
         "          </ul>" +
         "       </div>" +

      "    </div>" +
         "    <div ng-show='showButtons()'>" +
         "        <button type='button' class='btn btn-default btn-block' ng-if='recentlyViewedModel.showMoreRecentRecordsText' ng-click='showMoreRecentRecords()'>" +
         "           <span>Show more</span>" +
         "        </button>" +

      "         <button type='button' class='btn btn-default btn-block' ng-if='!(recentlyViewedModel.showMoreRecentRecordsText)' ng-click='showLessRecentRecords()'>" +
         "           <span>Show less</span>" +
         "        </button>" +
         "    </div>" +
         " </div>"
   });
})


/****************************************************************
 ** Directive to generate form with validation
 **
 ****************************************************************/
.directive("ipEntityListTable", function()
{
   return(
   {
      restrict: 'E',
      controller: EntityListTableController,
      scope:
      {
         parentModel: "=",
         loadStateModel: "=",
         currentRowIndex: "=",
         searchResults: "=",
         visibleFields: "=",
         staticTableData: "=",
         showCheckboxes: "=",
         showActions: "=",
         itemsPerPage: "=",
         currentPage: "=",
         pagesToDisplay: "=",
         email: "=",
         orderByField: "=",
         orderByDirection: "=",
         openNotesMethod: "&",
         checkboxClickedMethod: "&",
         selectAllPagesMethod: "&",
         rowClickMethod: "&",
         pageChangedMethod: "&",
         orderByChangedMethod: "&",
         executeQueryMethod: "&",
         isSortable: "@"
      },
      link: function(scope, element, attributes, ctrl)
      {
         if(scope.executeQueryMethod && scope.executeQueryMethod())
         {
            scope.executeQueryMethod()();
         }
      },
      templateUrl: qdimCoreBase + "app/templates/ip-entity-list-table.html"
   });
})


/****************************************************************
 ** Directive for primary image widget
 **
 ****************************************************************/
.directive("ipPrimaryImageEditor", function()
{
   return(
   {
      restrict: 'E',
      scope:
      {
         id: "=",
         fileConfig: "=",
         imageLabel: "=",
         boundEntity: "=",
      },
      controller: PrimaryImageEditorController,
      template: ' <ip-spinner id="primary-image-spinner-{{id}}" is-visible="loadStateModel.isStateLoading(primaryImageEditorModel)" hide-spinner="! loadStateModel.isStateLoadingSlow(primaryImageEditorModel)"> ' +
         '    <div ng-show="showImage"> ' +
         '       <div class="btn-group btn-inline right" style="position: absolute; right: 0px; top: -10px;" > ' +
         '          <button type="button" class="btn btn-transparent dropdown-toggle" data-toggle="dropdown"> ' +
         '             <i class="glyphicon glyphicon-cog"></i> ' +
         '             <span class="caret"></span> ' +
         '          </button> ' +
         '          <ul class="dropdown-menu flyout-left" role="menu"> ' +
         '             <li><a ng-click="openCropper()">Crop</a></li> ' +
         '             <li><a ng-click="deleteOnClick()">Delete</a></li> ' +
         '          </ul> ' +
         '       </div> ' +
         '       <label style="padding-bottom: 10px;"><span>{{imageLabel}}</span></label> ' +
         '       <img id="primary-image-{{id}}" style="border: 1px solid #B4B4B4; width: 100%; height: 100%" ng-src="{{fullFileName}}" /> ' +
         '    </div> ' +
         '    <div ng-if="! showImage"> ' +
         '       <ip-file-upload ' +
         '          files-selected-function="handleFilesSelected" ' +
         '          upload-complete-function="handleUploadComplete" ' +
         '          title="Attach {{imageLabel}}" ' +
         '          abort-function="handleAbort" ' +
         '          is-internal="false" ' +
         '          is-temporary="false" ' +
         '          file-name="{{entityFileName}}" ' +
         '       ></ip-file-upload> ' +
         '    </div> ' +
         ' </ip-spinner> '
   });
})


/****************************************************************
 ** Directive to generate file upload widget
 **
 ****************************************************************/
.directive("ipFileUpload", function()
{
   return(
   {
      restrict: 'E',
      controller: FileUploadController,
      scope:
      {
         title: "@",
         isInternal: "@",
         isTemporary: "@",
         abortFunction: "&",
         filesSelectedFunction: "&",
         uploadCompleteFunction: "&",
         fileName: "@",
      },
      templateUrl: qdimCoreBase + "app/templates/ip-file-upload.html"
   });
})


/****************************************************************
 ** Directive to generate form with validation
 **
 ****************************************************************/
.directive("ipForm", function()
{
   return(
   {
      restrict: 'E',
      controller: FormController,
      scope:
      {
         fields: "=",
         layout: "=",
         boundEntity: "=",
         isFormValid: "=",
         checkboxMode: "=",
         submitFunction: "&",
         placeholder: "=",
         qdimDictionary: '=',
         dictionaryKeyPrefix: '=',
         helpKeyword: '=',
         afterFieldCallback: "&",
         setValueInFieldCallback: "&",
         removeContextualGroupingClass: "="
      },
      link: function(scope, element, attributes, ctrl)
      {
         scope.isFormValid = false;
      },
      templateUrl: qdimCoreBase + "app/templates/ip-form.html"
   });
})


/****************************************************************
 ** Directive to generate rf form with validation
 **
 ****************************************************************/
.directive("ipRfForm", function()
{
   return(
   {
      restrict: 'E',
      controller: FormController,
      scope:
      {
         fields: "=",
         layout: "=",
         boundEntity: "=",
         isFormValid: "=",
         isView: "="
      },
      link: function(scope, element, attributes, ctrl)
      {
         scope.isFormValid = false;
      },
      templateUrl: qdimCoreBase + "app/templates/ip-rf-form.html"
   });
})


/****************************************************************
 ** Directive to generate form with validation
 **
 ****************************************************************/
.directive("ipViewForm", function()
{
   return(
   {
      restrict: 'E',
      scope:
      {
         sections: "=",
         fields: "=",
         boundEntity: "=",
         showSectionLabels: "=",
         allFullWidth: "="
      },
      templateUrl: qdimCoreBase + "app/templates/ip-view-form.html"
   });
})


/****************************************************************
 ** Directive to output a value in a standard way.
 **
 ****************************************************************/
.directive("ipValue", function()
{
   return(
   {
      restrict: 'E',
      scope:
      {
         type: "=",
         value: "=",
         uiElement: "=",
         label: "=",
         label: "=",
         linkPath: "="
      },
      templateUrl: qdimCoreBase + "app/templates/ip-value.html"
   });
})


/****************************************************************
 ** Directive to generate criteria filter dropdown
 **
 ****************************************************************/
.directive("ipDynamicPanel", function($compile)
{
   return(
   {
      restrict: 'E',
      scope:
      {
         panel: "=",
         panelIndex: "=",
         submitFunction: "&",
         cancelFunction: "&",
         canProceedFunction: "&",
         qdimDictionary: "="
      },
      link: function(scope, element, attributes, ctrl)
      {
         var newDirective = angular.element('<' + scope.panel + ' panel-index=' + scope.panelIndex + ' submit-function="submitFunction()" cancel-function="cancelFunction()" can-proceed-function="canProceedFunction()" qdim-dictionary="qdimDictionary"></' + scope.panel + '>');
         element.append(newDirective);
         $compile(newDirective)(scope);
      }
   });
})


/****************************************************************
 ** Directive to generate work panel
 **
 ****************************************************************/
.directive("ipRfWorkPanel", function($compile, processService)
{
   return(
   {
      restrict: 'E',
      controller: RFWorkPanelController,
      scope:
      {
         submitFunction: "&",
         cancelFunction: "&",
         canProceedFunction: "&",
         panelIndex: "="
      },
      templateUrl: qdimCoreBase + "app/templates/rf-work-panel.html"
   });
})


/****************************************************************
 ** Directive to generate inquiry panel
 **
 ****************************************************************/
.directive("ipRfInquiryPanel", function($compile, processService)
{
   return(
   {
      restrict: 'E',
      controller: RFInquiryPanelController,
      scope:
      {
         submitFunction: "&",
         panelIndex: "=",
         title: "@",
         inputField: "="
      },
      templateUrl: qdimCoreBase + "app/templates/rf-inquiry-panel.html"
   });
})


/****************************************************************
 ** Directive to generate criteria filter dropdown
 **
 ****************************************************************/
.directive("ipTextPanel", function($compile, processService)
{
   return(
   {
      restrict: 'E',
      controller: TextPanelController,
      scope:
      {
         panelIndex: "="
      },
      link: function(scope, element, attributes, ctrl)
      {
         var config = processService.getCurrentScreen().screenPanels[scope.panelIndex].config;
         scope.processService = processService

         //////////////////////////////////////////////////////////////////////////////////////
         // pick the style for the box - default to class=well, but accept a value of 'plain //
         //////////////////////////////////////////////////////////////////////////////////////
         var elementClassString;
         if(config.plain)
         {
            elementClassString = " ";
         }
         else
         {
            elementClassString = " class='well' ";
         }

         ////////////////////////////////////////////////////////////////////////////////////////
         // build html string - start with a col div, then additional div with optional class, //
         // then include or message, finally close the two divs                                //
         ////////////////////////////////////////////////////////////////////////////////////////
         var newElementHTML = '<div class="col-md-12"><div ' + elementClassString + '>';
         if(config.include)
         {
            newElementHTML += '<ng-include src="\'' + config.include + '\'"></ng-include>';
         }
         else
         {
            newElementHTML += config.message;
         }
         newElementHTML += '</div></div>';

         var newElement = angular.element(newElementHTML);

         element.append(newElement);
         $compile(newElement)(scope);
      }
   });
})


/****************************************************************
 ** Directive to generate a record-view panel
 **
 ****************************************************************/
.directive("ipRecordViewPanel", function($compile, processService)
{
   return(
   {
      restrict: 'E',
      controller: RecordViewPanelController,
      scope:
      {
         panelIndex: "="
      },
      link: function(scope, element, attributes, ctrl)
      {
         ///////////
         // no op //
         ///////////
      },
      templateUrl: qdimCoreBase + "app/templates/ip-record-view-panel.html"
   });
})


/****************************************************************
 ** Directive to generate an rf record view
 **
 ****************************************************************/
.directive("ipRfRecordView", function($compile, processService)
{
   return(
   {
      restrict: 'E',
      scope:
      {
         panelIndex: "=",
         submitFunction: "&"
      },
      link: function(scope, element, attributes, ctrl)
      {
         scope.processService = processService;
      },
      controller: RFRecordViewPanelController,
      templateUrl: qdimCoreBase + "app/templates/ip-rf-record-view.html"
   });
})


/****************************************************************
 ** Directive to generate an rf input
 **
 ****************************************************************/
.directive("ipRfInput", function($compile, $modalStack, processService, loadStateModel, processModel, rfInputModel)
{
   return(
   {
      restrict: 'E',
      scope:
      {
         submitFunction: "&"
      },
      controller: ['$rootScope', '$scope', '$timeout',
         function($rootScope, $scope, $timeout)
         {
            $scope.loadStateModel = loadStateModel;
            $scope.rfInputModel = rfInputModel;

            ////////////////////////////////
            // method when submit clicked //
            ////////////////////////////////
            $scope.submitClicked = function()
            {
               /////////////////////////
               //set to loading state //
               /////////////////////////
               $scope.loadStateModel.setStateLoading($scope.rfInputModel);

               ///////////////////////////////////////////////////
               //if Order inquiry app, only allow number values //
               ///////////////////////////////////////////////////
               if(processService.getCurrentScreen().screenPanels[0].config.screenLabel == "Order" && !$scope.rfInputModel.boundField.match(/^-?\d*\.\d$/))
               {
                  ////////////////////////////////////////////////
                  // only digits are allowed, remove non-digits //
                  ////////////////////////////////////////////////
                  $scope.rfInputModel.boundField = $scope.rfInputModel.boundField.replace(/^(-?)(\d*)(\.?)(\d*).*/g, '$1$2$3$4');
               }

               processService.postData['inputField'] = $scope.rfInputModel.boundField;

               /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
               // set recordId to null before calling submit function so that input, rather than recordId, will be used to fetch data //
               /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
               processService.postData['recordId'] = null;

               $scope.submitFunction()();
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // method to keep text input in focus IF NO modals open, solution found at:                                         //
            // http://stackoverflow.com/questions/6201278/how-to-select-all-text-in-textarea-on-focus-in-safari/6201757#6201757 //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $scope.isInputFocus = function()
            {
               if(!(!!$modalStack.getTop()))
               {
                  jQuery('#focusId').focus(function()
                  {
                     var $this = jQuery(this);
                     $this.select();

                     $timeout(function()
                     {
                        $this.select();
                     }, 1);

                     //////////////////////////////////
                     // Work around WebKit's problem //
                     //////////////////////////////////
                     function mouseUpHandler()
                     {
                        //////////////////////////////////////////
                        // Prevent further mouseup intervention //
                        //////////////////////////////////////////
                        $this.off("mouseup", mouseUpHandler);
                        return false;
                     }

                     $this.mouseup(mouseUpHandler);
                  });
               }
               else
               {
                  jQuery('#focusId').blur();
               }
            }
            $scope.isInputFocus();

            ////////////////////////////////////////////////////////////
            //when process step complete, set load state model to ok  //
            ////////////////////////////////////////////////////////////
            $rootScope.$on("jobStatusComplete", function()
            {
               ////////////////////////////////////////////////////////////
               // set load state to ok at beginning of each process step //
               ////////////////////////////////////////////////////////////
               $scope.loadStateModel.setStateOk($scope.rfInputModel);
            });

         }
      ],
      templateUrl: qdimCoreBase + "app/templates/ip-rf-input.html"
   });
})


/****************************************************************
 ** Directive to generate recently viewed (records) for rf apps
 **
 ****************************************************************/
.directive("ipRfRecent", function($compile, processService)
{
   return(
   {
      restrict: 'E',
      controller: RFRecentlyViewedController,
      scope:
      {
         limit: '@',
         submitFunction: "&",
         boundField: "="
      },
      templateUrl: qdimCoreBase + "app/templates/ip-rf-recent.html"
   });
})


/****************************************************************
 ** Directive to generate today's jobs
 **
 ****************************************************************/
.directive("ipRfRecordListSidebar", function($compile, processService)
{
   return(
   {
      restrict: 'E',
      controller: RFRecordListSidebarController,
      scope:
      {
         sectionTitle: '@',
         sectionLabel: '=',
         limit: '@',
         submitFunction: "&",
         boundField: "="
      },
      templateUrl: qdimCoreBase + "app/templates/ip-rf-record-list-sidebar.html"
   });
})


/****************************************************************
 ** Directive to generate rf record list
 **
 ****************************************************************/
.directive("ipRfRecordList", function($compile, processService)
{
   return(
   {
      restrict: 'E',
      link: function(scope, element, attributes, ctrl)
      {
         scope.rfMode = true;
         scope.processService = processService;
      },
      controller: RFRecordListPanelController,
      templateUrl: qdimCoreBase + "app/templates/ip-rf-record-list.html"
   });
})


/****************************************************************
 ** Directive to generate rf record select
 **
 ****************************************************************/
.directive("ipRfRecordSelectPanel", function()
{
   return(
   {
      restrict: 'E',
      controller: ['$scope', '$modal',
         function($scope, $modal)
         {
            var modalParams = {};
            modalParams.panelIndex = $scope.panelIndex;

            $modal.open(
            {
               templateUrl: qdimCoreBase + "app/templates/ip-rf-record-select-panel.html",
               controller: RFRecordSelectPanelController,
               resolve:
               {
                  modalParams: function()
                  {
                     return(modalParams);
                  }
               }
            });
         }
      ]
   });
})


/****************************************************************
 ** Directive to generate rf record edit
 **
 ****************************************************************/
.directive("ipRfRecordEdit", function()
{
   return(
   {
      restrict: 'E',
      controller: RFRecordEditPanelController,
      templateUrl: qdimCoreBase + "app/templates/ip-rf-record-edit.html"
   });
})


/****************************************************************
 ** Directive to generate rf record launch
 **
 ****************************************************************/
.directive("ipRfRecordLaunch", function()
{
   return(
   {
      restrict: 'E',
      templateUrl: qdimCoreBase + "app/templates/ip-rf-record-launch.html",
      controller: ['$scope', 'processService', 'qdimService',
         function($scope, processService, qdimService)
         {
            $scope.label = processService.definition.label;

            if(processService.payload.fields.rfDeleteSuccessMessage)
            {
               qdimService.addAlert($scope,
               {
                  msg: processService.payload.fields.rfDeleteSuccessMessage,
                  type: "success"
               });

               ////////////////////////////////////////////////////
               // clear out rfDeleteSuccessMessage after showing //
               ////////////////////////////////////////////////////
               processService.payload.fields.rfDeleteSuccessMessage = null;
            }
         }
      ]
   });
})


/****************************************************************
 ** Directive to generate process bulk edit form
 **
 ****************************************************************/
.directive("ipBulkEditFieldsPanel", function()
{
   return(
   {
      restrict: 'E',
      controller: FormPanelController,
      scope:
      {
         panelIndex: "=",
      },
      template: '  <ip-form ' +
         '     fields="processService.getCurrentScreen().screenPanels[panelIndex].config.fields" ' +
         '     layout="processService.getCurrentScreen().screenPanels[panelIndex].config.layout" ' +
         '     is-form-valid="processService.panelValidationList[processService.currentStep]" ' +
         '     checkbox-mode="true" ' +
         '     set-value-in-field-callback="processService.setValueInField" ' +
         '     bound-entity="processService.postData[processService.payload.fields.outputFieldName]"></ip-form> '
   });
})


/****************************************************************
 ** Directive to generate process form panels
 **
 ****************************************************************/
.directive("ipFormPanel", function()
{
   return(
   {
      restrict: 'E',
      controller: FormPanelController,
      scope:
      {
         panelIndex: "=",
         submitFunction: "&",
         qdimDictionary: "="
      },
      template: '  <ip-form ' +
         '     submit-function="submitFunction()" ' +
         '     fields="processService.definition.fields" ' +
         '     qdim-dictionary="qdimDictionary" ' +
         '     help-keyword="processService.definition.label " ' +
         '     dictionary-key-prefix="processService.appData.name + \'.\' + processService.definition.name + \'.\' + processService.getCurrentScreen().name" ' +
         '     layout="processService.getCurrentScreen().screenPanels[panelIndex].layout" ' +
         '     is-form-valid="processService.panelValidationList[\'panel\'+panelIndex]" ' +
         '     placeholder="processService.placeholderData" ' +
         '     set-value-in-field-callback="processService.setValueInField" ' +
         '     bound-entity="processService.postData"></ip-form> '
   });
})


/****************************************************************
 ** Directive to generate process record list panels
 **
 ****************************************************************/
.directive("ipRecordListPanel", function()
{
   return(
   {
      restrict: 'E',
      controller: RecordListPanelController,
      scope:
      {
         panelIndex: "="
      },
      template: '<div class="col-md-12">' +
         '  <ip-entity-list-table ' +
         '     show-spinner="false" ' +
         '     parent-model="recordListPanelModel" ' +
         '     load-state-model="recordListPanelModel.loadStateModel" ' +
         '     search-results="recordListPanelModel.searchResults" ' +
         '     visible-fields="processService.getFields()" ' +
         '     static-table-data="processService.staticTableData" ' +
         '     show-checkboxes="false" ' +
         '     show-actions="false" ' +
         '     items-per-page="recordListPanelModel.itemsPerPage" ' +
         '     current-page="recordListPanelModel.currentPage" ' +
         '     pages-to-display="recordListPanelModel.pagesToDisplay" ' +
         '     order-by-field="recordListPanelModel.orderByField" ' +
         '     order-by-direction="recordListPanelModel.orderByDirection" ' +
         '     execute-query-method="getEntities" ' +
         '     page-changed-method="pageChanged" ' +
         '     is-sortable="false" ' +
         ' </ip-entity-list-table> ' +
         '</div>'
   });
})


/****************************************************************
 ** Directive to generate process record list panels
 **
 ****************************************************************/
.directive("ipEntityActionListPanel", function()
{
   return(
   {
      restrict: 'E',
      controller: EntityActionListPanelController,
      scope:
      {
         panelIndex: "="
      },
      templateUrl: qdimCoreBase + "app/templates/entity-action-list-panel.html"
   });
})


/****************************************************************
 ** Directive to generate process results panels
 **
 ****************************************************************/
.directive("ipResultsPanel", function()
{
   return(
   {
      restrict: 'E',
      controller: ResultsPanelController,
      scope:
      {
         panelIndex: "="
      },
      templateUrl: qdimCoreBase + "app/templates/results-panel.html"
   });
})


/****************************************************************
 ** Directive to generate file upload panel
 **
 ****************************************************************/
.directive("ipFileUploadPanel", function()
{
   return(
   {
      restrict: 'E',
      controller: FileUploadPanelController,
      scope:
      {
         panelIndex: "="
      },
      templateUrl: qdimCoreBase + "app/templates/file-upload-panel.html"
   });
})


/****************************************************************
 ** Directive to generate file download panel
 **
 ****************************************************************/
.directive("ipFileDownloadPanel", function()
{
   return(
   {
      restrict: 'E',
      controller: FileDownloadPanelController,
      scope:
      {
         panelIndex: "="
      },
      templateUrl: qdimCoreBase + "app/templates/file-download-panel.html"
   });
})


/****************************************************************
 ** Directive to generate field mapper panels
 **
 ****************************************************************/
.directive("ipFieldMapperPanel", function()
{
   return(
   {
      restrict: 'E',
      controller: FieldMapperPanelController,
      scope:
      {
         panelIndex: "="
      },
      templateUrl: qdimCoreBase + "app/templates/field-mapper-panel.html"
   });
})


/****************************************************************
 ** Directive to generate favorite filters
 **
 ****************************************************************/
.directive("ipFavoriteFilters", function()
{
   return(
   {
      restrict: 'E',
      controller: FavoriteFiltersController,
      scope:
      {
         limit: '@'
      },
      template: " <div class='row'>" +
         "    <div class='col-xs-10'><h4 class='launcher-subhead'>Favorite Filters</h4></div>" +
         "    <div class='col-xs-2'>" +
         "       <div class='launcher-subhead widget-settings'>" +
         "          <div data-toggle='dropdown'><i class='ip-glyphs gear widget-settings'></i></div> " +
         "          <ul class='dropdown-menu flyout-left' role='menu'> " +
         "             <li><a href='" + pathPrefix + "/qdim-core/qdim-manager/user-filter'>Manage Filters</a></li>" +
         "          </ul>" +
         "       </div>" +
         "    </div>" +

      " </div>" +
         " <hr class='section-rule widget'>" +
         " <div class='collapse in recently-viewed' id='favorite-filters'>" +
         "    <div class='col-md-12'>" +

      "       <div class='row' ng-show='loadStateModel.isStateError(favoriteFiltersModel)'>" +
         "          <div class='alert alert-danger no-hover'>Oops! Troubles loading your filters right now...</div>" +
         "       </div>" +

      "       <div class='row' ng-show='loadStateModel.isStateLoadingSlow(favoriteFiltersModel)'>" +
         "          <div class='loading-wrapper'><img class='loading' src='" + webCoreBase + "/img/spinner-white.gif'> <span class='loading-text'>Your filters are loading...</span></div>" +
         "       </div>" +

      "       <div class='row' ng-show='loadStateModel.isStateEmpty(favoriteFiltersModel)'>" +
         "          You haven't marked any filters as your favorites. When you do, they will appear here." +
         "       </div>" +

      "       <div class='row'>" +
         "          <ul class='list-unstyled list-block' ng-show='loadStateModel.isStateOk(favoriteFiltersModel)'>" +
         "             <li class='col-md-6' ng-repeat='favoriteFilter in favoriteFiltersModel.favoriteFilters | limitTo: favoriteFiltersModel.clickLimit track by $index'>" +
         "                <div class='favorite-button'>" +
         "                   <input type='checkbox' name='{{favoriteFilter.id}}' ng-model='favoriteFiltersModel.isFavoriteFilter[favoriteFilter.id]'" +
         "                     ng-click='changeFavoriteFilter(favoriteFilter.id, favoriteFilter.entityId, favoriteFilter.entityTypeName)' class='favorite-button-checkbox' id='favoriteFilter{{favoriteFilter.id}}'>" +
         "                       <label class='favorite-button-label' for='favoriteFilter{{favoriteFilter.id}}'>" +
         "                          <i ng-if='(favoriteFiltersModel.isFavoriteFilter[favoriteFilter.id])' class='glyphicon glyphicon-filter on' style='color: #05609A;' tooltip='Remove from Filters' tooltip-placement='top'></i>" +
         "                          <i ng-if='!(favoriteFiltersModel.isFavoriteFilter[favoriteFilter.id])' class='glyphicon glyphicon-filter off' style='color: #D0E0EA;' tooltip='Add to Filters' tooltip-placement='top'></i>" +
         "                       </label>" +
         "                 </div>" +

      "                 <div>" +
         "                    <div ng-if='! favoriteFilter.userFilterCount' style='float: right; margin-left: 15px;'>--</div> " +
         "                    <div ng-if='favoriteFilter.userFilterCount == 0' style='float: right; margin-left: 15px;'>0</div> " +
         "                    <div ng-if='favoriteFilter.userFilterCount' style='float: right; margin-left: 15px;'>{{favoriteFilter.userFilterCount | number}}</div> " +
         "                    <a style='width: auto;' href='" + pathPrefix + "{{favoriteFilter.path}}'><span class='apps-links-name ng-binding'>{{favoriteFilter.entityLabel}}</span></a>" +
         "                 </div>" +

      "             </li>" +
         "          </ul>" +
         "       </div>" +
         "    </div>" +

      "    <div ng-show='showButtons()'>" +
         "        <button type='button' class='btn btn-default btn-block' ng-if='favoriteFiltersModel.showMoreFavoriteFiltersText' ng-click='showMoreFavoriteFilters()'>" +
         "           <span >Show More</span>" +
         "        </button>" +

      "        <button type='button' class='btn btn-default btn-block' ng-if='!(favoriteFiltersModel.showMoreFavoriteFiltersText)' ng-click='showLessFavoriteFilters()'>" +
         "           <span>Show Less</span>" +
         "        </button>" +
         "    </div>" +
         " </div>",

      link: function(scope, element, attributes, ctrl)
      {
         scope.getData(attributes.limit);
      }
   });
})


/****************************************************************
 ** Directive to generate tags section of pages
 **
 ****************************************************************/
.directive("ipTags", function()
{
   return(
   {
      restrict: 'E',
      controller: TagsController,
      scope:
      {
         limit: '@limit'
      },
      template: " <div class='row'> " +
         "    <div class='col-xs-10'><h4 class='launcher-subhead section-title'>Tags</h4></div> " +
         " </div> " +
         " <hr class='section-rule widget'> " +
         " <div class='row collapse in' id='tag'> " +
         "    <div class='col-md-12'> " +
         "       <div ng-show='loadStateModel.isStateLoadingSlow(tagsModel)'>" +
         "          <div class='loading-wrapper'><img class='loading' src='" + webCoreBase + "/img/spinner-white.gif'> <span class='loading-text'>Your tags are loading...</span></div>" +
         "       </div>" +

      "       <div ng-show='loadStateModel.isStateEmpty(tagsModel)'>" +
         "          Don't worry, once you have some tags setup they will appear here." +
         "       </div>" +

      "       <div ng-show='loadStateModel.isStateError(tagsModel)'>" +
         "          <div class='alert alert-danger no-hover'><strong>Oops! </strong>Troubles loading your tags right now...</div>" +
         "       </div>" +
         "       <ul ng-show='loadStateModel.isStateOk(tagsModel)' class='list-inline'> " +
         "          <li ng-repeat='tag in tagsModel.tags' ng-click='tagOnClick(tag)'><a class='btn btn-tag'>{{tag}}</a></li> " +
         "       </ul> " +
         "    </div> " +
         " </div> "
   });
})


/****************************************************************
 ** Directive to output a single form field
 **
 ****************************************************************/
.directive("ipFormField", function()
{
   return(
   {
      restrict: 'E',
      controller: ['$scope', '$http',
         function($scope, $http)
         {
            /***************************************************************
             ** getPossibleValueList
             **
             ** ajax to get possible value list
             ***************************************************************/
            $scope.getPossibleValueList = function(url)
            {
               $http.get(url + "search?searchText=&limit=25&startAt=1")
                  .then($scope.getPossibleValueListSuccess, $scope.getPossibleValueListError);
            };


            /***************************************************************
             ** getPossibleValueListSuccess
             **
             ** success
             ***************************************************************/
            $scope.getPossibleValueListSuccess = function(result)
            {
               $scope.possibleValueList = result.data.possibleValueList;
            };


            /***************************************************************
             ** getPossibleValueListError
             **
             ** error
             ***************************************************************/
            $scope.getPossibleValueListError = function(url) {

            };

         }
      ],
      templateUrl: qdimCoreBase + "app/templates/ip-form-field.html"
   });
})


/****************************************************************
 ** Directive to output a single rf form field
 **
 ****************************************************************/
.directive("ipRfFormField", function()
{
   return(
   {
      restrict: 'E',
      controller: ['$scope', '$http',
         function($scope, $http)
         {
            /***************************************************************
             ** getPossibleValueList
             **
             ** ajax to get possible value list
             ***************************************************************/
            $scope.getPossibleValueList = function(url)
            {
               $http.get(url + "search?searchText=&limit=25&startAt=1")
                  .then($scope.getPossibleValueListSuccess, $scope.getPossibleValueListError);
            };


            /***************************************************************
             ** getPossibleValueListSuccess
             **
             ** success
             ***************************************************************/
            $scope.getPossibleValueListSuccess = function(result)
            {
               $scope.possibleValueList = result.data.possibleValueList;
            };


            /***************************************************************
             ** getPossibleValueListError
             **
             ** error
             ***************************************************************/
            $scope.getPossibleValueListError = function(url) {

            };

         }
      ],
      templateUrl: qdimCoreBase + "app/templates/ip-rf-form-field.html"
   });
})


/****************************************************************
 ** Directive to output a standard container for next-step buttons
 **
 ****************************************************************/
.directive("ipNextSteps", function()
{
   return(
   {
      restrict: 'E',
      transclude: true,
      template: '  <div id="nextSteps">' +
         '     <ul class="list-unstyled icon-link-list" ng-transclude></ul>' +
         '  </div>'
   });
})


/****************************************************************
 ** Directive to output a standard next-step button
 **
 ****************************************************************/
.directive("ipNextStepButton", function()
{
   return(
   {
      restrict: 'E',
      controller: ['$scope', 'qdimService',
         function($scope, qdimService)
         {
            /***************************************************************
             ** get app icon
             **
             ***************************************************************/
            $scope.getAppIcon = function()
            {
               return(qdimService.getCurrentAppIcon());
            };

         }
      ],
      scope:
      {
         label: '@',
         href: '@'
      },
      template: '<li>' +
         '      <a class="" href="{{href}}">' +
         '         <i class="ip-glyphs {{getAppIcon()}}"></i>' +
         '         <span class="link-list-label">{{label}}</span>' +
         '      </a>' +
         '</li>'
   });
})


;
